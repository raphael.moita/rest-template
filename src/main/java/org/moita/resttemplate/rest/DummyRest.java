package org.moita.resttemplate.rest;

import lombok.extern.slf4j.Slf4j;
import org.moita.resttemplate.domain.Dummy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.Arrays.asList;

@RestController
@RequestMapping("/dummy")
@Slf4j
public class DummyRest
{
    @GetMapping("/people")
    public List<Dummy> dummies()
    {
        log.info("Getting dummies from source");
        return asList(
                Dummy.builder().name("Rapha").surname("Moita").gender("M").age(44).build(),
                Dummy.builder().name("Mari").surname("Moita").gender("F").age(38).build());
    }
}
