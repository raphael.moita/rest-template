package org.moita.resttemplate.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;

import static org.springframework.http.HttpStatus.Series.CLIENT_ERROR;
import static org.springframework.http.HttpStatus.Series.SERVER_ERROR;

@Slf4j
public class RestTemplateResponseErrorHandler implements ResponseErrorHandler
{
    @Override
    public boolean hasError(ClientHttpResponse httpResponse) throws IOException
    {
        return (httpResponse.getStatusCode().series() == CLIENT_ERROR
                || httpResponse.getStatusCode().series() == SERVER_ERROR);
    }

    @Override
    public void handleError(ClientHttpResponse httpResponse) throws IOException
    {
        if (httpResponse.getStatusCode().series() == SERVER_ERROR)
        {
            // handle SERVER_ERROR
            log.error("Server error: {}", httpResponse);
        }
        else if (httpResponse.getStatusCode().series() == CLIENT_ERROR)
        {
            // handle CLIENT_ERROR
            log.error("Client error: {}", httpResponse);
        }
    }
}